#!/bin/sh

memesave() {
	curl -s https://www.memeatlas.com/$1.html  | awk -F 'href=\"' '{print $2}' | awk -F 'src=\".*loading' '{print $1}' | awk -F '\"><img' '{print $1}' | grep 'images/'$2 > $1.txt;
	cat $1.txt | while read l; do curl -sO "https://www.memeatlas.com/${l}"; done;
	shred -zu $1.txt;
}
memedir() { mkdir $2 && cd $2 &&  memesave $1 $2 && cd ..; }

MEME_DIR="rarememes"
mkdir $MEME_DIR && cd $MEME_DIR
echo ""
echo ":] DOWNLOADING MEMES ..."
memedir "BAT-memes" "bat"
memedir "bobo-memes" "bobos"
memedir "boomer-memes" "boomers"
memedir "brainlet-memes" "brainlets"
memedir "grug-memes" "grugs"
memedir "reaction-memes" "misc"
memedir "pepe-memes" "pepes"
memedir "wojak-memes" "wojaks"
memedir "zoomer-memes" "zoomers"
echo ""
echo "DONE ..."
